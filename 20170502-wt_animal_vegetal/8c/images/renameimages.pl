#!/bin/env perl

use strict;
use warnings;

die "Map file already exists! Aborting renameimages.pl\n" if(-e 'imgmap.tsv');
open my $mfh, '>', 'imgmap.tsv' or die $!;
foreach my $ofn (glob("*.tif")){
    my @f = split(/-/, $ofn);
    my $tail = join('-', @f[1..$#f]);
    $tail =~ s/\.tif$//;
    $tail =~ s/\s+/_/g;
    my $nfn = join('_', $tail, $f[0]).'.tif';
    $ofn =~ s/(\s)/\\$1/g;
    system("mv $ofn $nfn");
    print $mfh join("\t", $ofn, $nfn)."\n";
}
close $mfh;
