#!/bin/env python

import sys
from scipy.spatial import distance

c1 = []
with open(sys.argv[1], 'r') as f1:
    for line in f1:
        line = line.strip()
        x, y, z = line.split("\t")
        c1.append([float(x),float(y),float(z)])
c2 = []
with open(sys.argv[2], 'r') as f2:
    for line in f2:
        line = line.strip()
        x, y, z = line.split("\t")
        c2.append([float(x),float(y),float(z)])
for a in c1:
    shortest = 1e100
    for b in c2:
        dist = distance.euclidean(a,b)
        if dist < shortest:
            shortest = dist
    print(shortest)
