#!/bin/env perl

use strict;
use warnings;

foreach my $orig (@ARGV){
    my $new = $orig;
    $new =~ s/\s/_/g;
    $orig =~ s/\s/\\ /g;
    #print "$orig\n";
    system("mv $orig $new");
}
