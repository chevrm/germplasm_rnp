#!/bin/env perl

use strict;
use warnings;

my $basedir = 'src2';
my $channel1pref = 'C1-late_4cel_cortex_189x';
my $channel2pref = 'C2-late_4cel_cortex_189x';
my $info = 'info.txt';

my %dim = ();
open my $ifh, '<', "$basedir/$info" or die $!;
while(<$ifh>){
    chomp;
    if($_ =~ m/^(\w+):\s+(\d+\.*\d*)\s\S+\s\((\d+)\)$/){
	$dim{lc($1)}{'um'} = $2;
	$dim{lc($1)}{'px'} = $3;
    }
}
close $ifh;

my $um_per_px_x = $dim{'width'}{'um'} / $dim{'width'}{'px'};
my $um_per_px_y = $dim{'height'}{'um'} / $dim{'height'}{'px'};
my $um_per_slice = $dim{'depth'}{'um'} / $dim{'depth'}{'px'};

my %zlevel = ();
foreach my $slice (0..$dim{'depth'}{'px'}-1){
    $zlevel{$slice} = $dim{'depth'}{'um'} - ($um_per_slice * $slice);
}

my @c1 = glob("$basedir/$channel1pref*.tif");
my @c2 = glob("$basedir/$channel2pref*.tif");

for(my $i=0;$i<scalar(@c1);$i+=1){
    my $args = join(' ', $c1[$i], $c2[$i], $um_per_px_x, $um_per_px_y, $zlevel{$i});
    system("python rnp_striper.py $args");
}
