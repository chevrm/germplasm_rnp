#!/bin/env perl

use strict;
use warnings;

my $rnp = '/home/mchevrette/git/germplasm_rnp/rnp_striper.py';
my $qcf = 'manual_qc.tsv';
open my $qfh, '<', $qcf or die $!;
my %done = ();
while(<$qfh>){
    chomp;
    next if($_ =~ m/^#/);
    my @qc = split(/\t/, $_);
    if($qc[1] eq 'Pass'){
	my @fn = glob("images/$qc[0]*.tif");
	foreach my $first (@fn){
	    foreach my $second (@fn){
		next if($first eq $second);
		next if(exists $done{$first}{$second} || exists $done{$second}{$first});
		my $out = join('.', $first, 'vs', $second, 'tsv');
		$out =~ s/\.tif//g;
		$out =~ s/images\///g;
		print "Processing $out...\n";
		system("python $rnp $first $second > $out");
		$done{$first}{$second} = 1;
		$done{$second}{$first} = 1;
	    }
	}
    }
}
close $qfh;
