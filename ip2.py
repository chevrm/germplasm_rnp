#!/bin/env python

import sys, math
from scipy.spatial.distance import pdist
from math import sqrt
from math import tan
import numpy as np
import matplotlib.pyplot as plt

from skimage.feature import blob_dog
from skimage.filters import sobel
from scipy import ndimage as ndi
from skimage import morphology
from skimage.feature import canny
from skimage.color import label2rgb



sds = 1

def is_bottom(ps, point, mid):
    y_on_line = (ps * (mid[0] - point[0])) + point[1]
    if(y_on_line > mid[1]):
        return True
    else:
        return False

midpoint = []
bestslope, bestdif = 0,-1
topbot = {}
sqrad = 0
seg = {}
labeled = {}

print "\t".join(['source', 'x', 'y', 'side', 'nhood', 'tot_intensity', 'avg_intensity', 'dist2mid'])
for ai in range(1, len(sys.argv)):
    if(ai == 0):
        continue
    ## Read in tiff and make a copy
    origimg = plt.imread(sys.argv[ai])
    image = plt.imread(sys.argv[ai])
    if(ai == 1):
        ## Flatten image array and get cutof
        allval = np.hstack(image)
        cut = allval.mean() + allval.std()*sds
        ## Grab that germplasm!
        ## Sobel elevation map
        elevation_map = sobel(image)
        ## Define markers
        markers = np.zeros_like(image)
        markers[image < allval.mean()] = 1
        markers[image > cut] = 2
        ## Watershed segmentation
        seg = morphology.watershed(elevation_map, markers)
        ## Fill in holes
        fill_h = ndi.binary_fill_holes(seg - 1)
        ## Get rid of small stuff
        size_thresh = float(image.shape[1] + image.shape[0])/2 * .35
        seg = morphology.remove_small_objects(fill_h, size_thresh)
        ## Define label
        labeled, _ = ndi.label(seg)
    ## Label based on segmentation
    image_label_overlay = label2rgb(labeled, image=image)
    ## Create new image based on labeled segmentation
    cleanimg = image
    cleanimg[labeled == 0] = 0
    ## Plot it out
    fig, axes = plt.subplots(1, 2, figsize=(5, 2.5), sharey=True)
    axes[0].imshow(image, cmap=plt.cm.gray, interpolation='nearest')
    axes[0].contour(seg, [0.5], linewidths=1.2, colors='y')
    axes[0].set_title('Watershed Segmentation')
    axes[1].imshow(image_label_overlay, interpolation='nearest')
    axes[1].set_title('Labeled Segments')
    for a in axes:
        a.axis('off')
        a.set_adjustable('box-forced')
    plt.tight_layout()
    plt.savefig(sys.argv[ai]+'.seg.jpg')
    fig, axes = plt.subplots(1, 2, figsize=(5, 2.5), sharey=True)
    axes[0].imshow(origimg, interpolation='nearest')
    axes[0].set_title('Original Image')
    axes[1].imshow(cleanimg, interpolation='nearest')
    axes[1].set_title('Cleaned GermPlasm')
    for a in axes:
        a.axis('off')
        a.set_adjustable('box-forced')
    plt.savefig(sys.argv[ai]+'.cleanup.jpg')
    if(ai == 1):
        ## Difference of gaussian blob detection
        blobs_dog = blob_dog(cleanimg, max_sigma=100, threshold=.25)
        ## Get radius
        blobs_dog[:, 2] = blobs_dog[:, 2] * sqrt(2)
        ## Define biggest blob
        max_x, max_y, max_r = 0,0,0
        for y, x, r in blobs_dog:
            if(r>max_r):
                max_x, max_y, max_r = x, y, r
        #print "\t".join([str(max_x), str(max_y), str(max_r)])
        sqrad = max_r / 10
        ## Plot
        fig,ax = plt.subplots()
        ax.set_title('Difference of Gaussian')
        ax.imshow(cleanimg, interpolation='nearest')
        plt.scatter(max_x,max_y, marker='*', color='r', s=400)
        for y, x, r in blobs_dog:
            c = plt.Circle((x, y), r, color='r', linewidth=2, fill=False)
            ax.add_patch(c)
        ax.set_axis_off()
        plt.savefig(sys.argv[ai]+'.dog.jpg')
        ## Find slope
        midpoint = [max_x, max_y]
        for ang in range(2,180,2):
            if(ang == 90):
                continue
            else:
                tb = {}
                slope = math.tan(math.radians(ang))
                for x in range(0,cleanimg.shape[1]):
                    for y in range(0,cleanimg.shape[0]):
                        if(cleanimg[y][x] > 0):
                            side = 'top'
                            p = [x,y]
                            if(is_bottom(slope, p, midpoint)):
                                side = 'bot'
                            if side in tb.keys():
                                tb[side] += 1
                            else:
                                tb[side] = 1
                if(bestdif == -1):
                    bestslope = slope
                    bestdif = abs(tb['top'] - tb['bot'])
                else:
                    d = abs(tb['top'] - tb['bot'])
                    if(d < bestdif):
                        bestslope = slope
                        bestdif = d
        bestslope = -1/bestslope
    ## Process each image
    for x in range(0,cleanimg.shape[1]):
        for y in range(0,cleanimg.shape[0]):
            if(cleanimg[y][x] > 0):
                side = 'top'
                p = [x,y]
                if(is_bottom(bestslope, p, midpoint)):
                    side = 'bot'
                if ai in topbot.keys():
                    if side in topbot[ai].keys():
                        topbot[ai][side] += 1
                    else:
                        topbot[ai][side] = 1
                else:
                    topbot[ai] = {}
                    topbot[ai][side] = 1
                lb = x-sqrad
                rb = x+sqrad
                ub = y+sqrad
                bb = y-sqrad
                if(bb < 0):
                    bb = 0
                if(lb < 0):
                    lb = 0
                if(rb > cleanimg.shape[1]):
                    rb = cleanimg.shape[1]
                if(ub > cleanimg.shape[0]):
                    ub = cleanimg.shape[0]
                totint = 0
                nint = 0
                for xs in range(int(lb),int(rb)):
                    for ys in range(int(bb), int(ub)):
                        nint += 1
                        totint += cleanimg[ys][xs]
                avgint = float(totint) / nint
                pm = np.array([[x,y], midpoint])
                dist2mid = pdist(pm)[0]
                print "\t".join([str(sys.argv[ai]), str(x), str(y), side, str(nint), str(totint), str(avgint), str(dist2mid)])

