#!/bin/env python

from __future__ import division
import sys, os, glob, math
import numpy as np
from PIL import Image

from skimage.filters import sobel
from skimage import segmentation
from skimage import morphology
from scipy import ndimage as ndi
from skimage.feature import blob_log

import matplotlib
matplotlib.use('Agg')
from matplotlib import pyplot as plt
#matplotlib.style.use('ggplot')
import seaborn as sb

sys.setrecursionlimit(10000) ## Default ceiling is 1000

def read_info(info_txt):
    um_per_px_x, um_per_px_y, um_per_z, n_z, z_depth = 0,0,0,0,0
    with open(info_txt, 'r') as ifh:
        for line in ifh:
            if line.startswith('Width:'):
                l = line.rstrip().split()
                um_per_px_x = float(l[1]) / int(l[3][1:-1])
            elif line.startswith('Height:'):
                l = line.rstrip().split()
                um_per_px_y = float(l[1]) / int(l[3][1:-1])
            elif line.startswith('Depth:'):
                l = line.rstrip().split()
                n_z = int(l[3][1:-1])
                z_depth = float(l[1])
                um_per_z = z_depth / n_z
    zlevel = {}
    for zslice in range(n_z):
        zlevel[zslice] = z_depth - (um_per_z * (zslice+1) )
    return um_per_px_x, um_per_px_y, zlevel

def get_rnp(tifs, group, sample, channel, um_per_px_x, um_per_px_y, zlevel):
    for z, tif in enumerate(tifs):
        #print("\t".join([group, sample, channel, 'z='+str(z)]))
        pref = os.path.splitext(tif)[0]
        original = np.array(Image.open(tif))
        ## Define segmentation threshold
        nonzero = original.copy()
        nonzero = nonzero[nonzero > 0].ravel()
        seg_cut = nonzero.mean() + (1.5 * nonzero.std())
        ## Sobel elevation map
        image = original.copy()
        elevation_map = sobel(image)
        ## Define markers
        markers = np.zeros_like(image)
        markers[image < nonzero.mean()] = 1
        markers[image > seg_cut] = 2
        ## Watershed segmentation
        seg = segmentation.watershed(elevation_map, markers)
        ## Fill in holes
        fill_h = ndi.binary_fill_holes(seg - 1)
        ## Get rid of small stuff (below axis fraction threshold)
        size_thresh = float(image.shape[1] + image.shape[0])/2 * 0.25
        seg = morphology.remove_small_objects(fill_h, size_thresh)
        ## Remove non-labeled points
        labels, _ = ndi.label(seg)
        image[labels == 0] = 0
        cleanimg = image.copy()
        ## Find blobs
        image, blob_coords, blobs_log = log(image, 0.000001)
        ## Plot
        fig, ax = plt.subplots(ncols=5,
                               figsize=(40,10),
                               sharex=True, sharey=True)
        ax[0].set_title('Original image')
        sb.heatmap(original, square=True, ax=ax[0], cbar=False)
        ax[1].set_title('Watershed segmentation')
        sb.heatmap(seg, square=True, ax=ax[1], cbar=False)
        ax[2].set_title('Cleaned image')
        sb.heatmap(cleanimg, square=True, ax=ax[2], cbar=False)
        ax[3].set_title('Laplacian of Gaussian')
        sb.heatmap(cleanimg, square=True, ax=ax[3], cbar=False)
        for y, x, r in blobs_log:
            col = 'white'
            if(image[int(y)][int(x)] > 0):
                col = 'yellow'
            c = plt.Circle((x, y), r, color=col, linewidth=1, fill=False)
            ax[3].add_patch(c)
        ax[4].set_title('Highest intensity blobs')
        sb.heatmap(image, square=True, ax=ax[4], cbar=False)
        plt.xlim(0,original.shape[0])
        plt.ylim(original.shape[1],0)
        plt.savefig(pref+'_clean.png', dpi=600)
        plt.close()
        for px_x in blob_coords:
            um_x = um_per_px_x * float(px_x)
            for px_y in blob_coords[px_x]:
                um_y = um_per_px_y * float(px_y)
                um_r = um_per_px_y * float(blob_coords[px_x][px_y])
                print("\t".join([group, channel, sample, str(um_x), str(um_y), str(zlevel[z]), str(um_r)]))
    return 1

def log(img, thresh):
    min_sig = 2 ## keep low to detect small blobs
    max_sig = 4 ## keep high to detect large blobs
    log_small_step = 0.000001
    log_big_step = 0.00005
    log_delta = 10
    log_blobs_max = 500
    blob_keep_zscore = 0.7

    ## Laplacian of gaussian blob detection
    blobs_log = blob_log(img, min_sigma=min_sig, max_sigma=max_sig, num_sigma=10, threshold=thresh)
    ## Get radius
    blobs_log[:, 2] = blobs_log[:, 2] * math.sqrt(2)
    blob_num = 0
    for y, x, r in blobs_log:
        blob_num += 1
    if(blob_num > log_blobs_max+log_delta):
        thresh += log_big_step
        return(log(img, thresh))
    elif(blob_num > log_blobs_max):
        thresh += log_small_step
        #sys.stderr.write("Blob num too high ("+str(blob_num)+"), trying thresh= "+str(thresh)+"\n")
        return(log(img, thresh))
    else:
        #sys.stderr.write("Blobs within range: "+str(blob_num)+"\n")
        ## Reorder the blob array
        blob_trans = {}
        top_blob = 0
        ai = np.array([])
        for y, x, r in blobs_log:
            r_int = int(round(r))
            x_lb, x_ub = int(x-r_int), int(x+r_int)
            y_lb, y_ub = int(y-r_int), int(y+r_int)
            t, n = 0, 0
            for x_pos in range(x_lb, x_ub+1):
                for y_pos in range(y_lb, y_ub+1):
                    if(y_pos < img.shape[0]):
                        if(x_pos < img.shape[1]):
                            t += img[y_pos][x_pos]
                            n += 1
            center = str(x)+','+str(y)
            blob_trans[center] = {}
            blob_trans[center]['avg_int'] = float(t)/n
            ai = np.append(ai, blob_trans[center]['avg_int'])
            if( blob_trans[center]['avg_int'] > top_blob ):
                top_blob = blob_trans[center]['avg_int']
            blob_trans[center]['x'] = x
            blob_trans[center]['y'] = y
            blob_trans[center]['r'] = r_int
            blob_trans[center]['t'] = t
            blob_trans[center]['n'] = n
        blob_trans_sort = sorted(
            blob_trans.items(),
            key=lambda x: (-x[1]['avg_int'])
        )
        ai_cutoff = np.mean(ai) + (np.std(ai) * blob_keep_zscore)
        blob_keeps = np.zeros_like(img)
        blob_keeps_coords = {}
        i = 1
        ymax, xmax = img.shape[0], img.shape[1]
        for c, l in blob_trans_sort:
            if(l['avg_int'] >= ai_cutoff ):
                if(l['x'] in blob_keeps_coords):
                    blob_keeps_coords[l['x']][l['y']] = l['r']
                else:
                    blob_keeps_coords[l['x']] = {}
                    blob_keeps_coords[l['x']][l['y']] = l['r']
                x_lb, x_ub = int(l['x']-l['r']), int(l['x']+l['r'])
                y_lb, y_ub = int(l['y']-l['r']), int(l['y']+l['r'])
                for x_pos in range(x_lb, x_ub+1):
                    for y_pos in range(y_lb, y_ub+1):
                        if (y_pos < ymax) and (x_pos < xmax):
                            blob_keeps[y_pos][x_pos] = img[y_pos][x_pos]
                i += 1
        #sys.stderr.write("\t".join(['thresh', str(thresh)])+"\n")
        #sys.stderr.write("\t".join(['blobs', str(blob_num)])+"\n")
    return blob_keeps, blob_keeps_coords, blobs_log


rnp = []
for txt in sorted(glob.glob("*/*/*txt")):
    um_per_px_x, um_per_px_y, zlevel = read_info(txt)
    p = os.path.split(txt)
    dazl = sorted(glob.glob(p[0]+"/dazl/*.tif"))
    vasa = sorted(glob.glob(p[0]+"/vasa/*.tif"))
    if len(dazl) != len(vasa):
        sys.exit("ERROR: Unequal tifs in set\t"+txt)
    p = txt.split('/')
    get_rnp(dazl, p[0], p[1], 'dazl', um_per_px_x, um_per_px_y, zlevel)
    get_rnp(vasa, p[0], p[1], 'vasa', um_per_px_x, um_per_px_y, zlevel)
