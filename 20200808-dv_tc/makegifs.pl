#!/bin/env perl

use strict;
use warnings;

my $t = 76;
mkdir 'gif' unless(-d 'gif');
foreach my $p0 (glob("*/*/*/*0000_clean.png")){
    my @p = split(/\//, $p0);
    my $grp = $p[0];
    my $channel = $p[2];
    my $sample = $p[1];
    my $allpng = join('/', @p[0..2], '*_clean.png');
    my $out = 'gif/'.join('.', $grp, $sample, $channel, 'gif');
    system("ffmpeg -framerate 3 -pattern_type glob -i '$allpng' -r 15 -threads $t -vf scale=5120:-1 $out") unless(-e $out);
}
